#!/usr/bin/python
"""Class Hasher used to compression match data from dota2api before that have store in DataBase(DB), 
and decopression data from DB to source format

Examples:
    # take a match ( for example from dota2api)
    import dota2api
    api = dota2api.Initialise("steam_api_key")
    match = api.get_match_details(match_id=2919000874)

    # match is a dict() object
    hasher = Hasher()
    # compresed_match == list()
    compresed_match = hasher(match)

    # decompresed_match == dict()
    decompresed_match = hasher(compresed_match)

    warning: decompresed_match has not some keys of source match 
"""

class Hasher(object):

    def __init__(self, actual_api_version = 2):
        """Make an object that have methods to compress and decompress 
        data from dota2api

        Keyword arguments:
        actual_api_version -- dota2api version

        Import param:
        api_version_keys -- match_json keys that need to save it 
                            and byte param for safety pack them  
        """
        from hasher_vars import api_version_keys
        self.api_version_keys = api_version_keys
        self.actual_api_version = actual_api_version

    def _debag_compress(self, data, version, data_key, type_error):
        """Debag match data convert """
        print('debug')
        for i, key in enumerate(self.api_version_keys[version][data_key]):
            if self.api_version_keys[version]['byte_codes'][key] == type_error:
                try:
                    pack(type_error , data[i])
                except Exception:
                    print(key, data[i])
                    raise 'struct error'
        print('debug end without exceptions')

    def _decompressed_version_upgrade(self, match_json):
        """ Include in match json file data_key and value to satisfied actual api version"""
        radiant_score = 0
        dire_score = 0
        for player in match_json['players']:
            player['backpack_0'] = 0
            player['backpack_1'] = 0
            player['backpack_1'] = 0
            player['scaled_hero_damage'] = player['hero_damage']
            player['scaled_tower_damage'] = player['tower_damage']
            player['scaled_hero_healing'] = player['hero_healing']
            ### radiant = [0,4] dire =[128,132]
            if player['player_slot'] < 5:
                radiant_score += player['kills']
            else:
                dire_score += player['kills']
        match_json['radiant_score'] = radiant_score
        match_json['dire_score'] = dire_score


    def compress(self, match_json):
        """Convert match json file to byte list"""
        # TODO vkotelnikov: Temporary patch ( need a custom avto-detection dota2api version unit)
        from struct import pack
        if not 'radiant_score' in match_json:
            version_loc = 1
            # print('default api version changed to', version_loc)
        else:
            version_loc = 2
            # print('default api version changed to', version_loc)
        res = []
        res.append(pack('B' , version_loc))
        match_data = []
        for key in self.api_version_keys[version_loc]['match_keys']:
            match_data.append(match_json[key])
        ### start ###
        ##################################################################################################
        pack_param = ''
        for key in self.api_version_keys[version_loc]['match_keys']:
            pack_param += self.api_version_keys[version_loc]['byte_codes'][key]
        ##################################################################################################
        ### end ###
        # pack_param = self.api_version_keys[version_loc]['match_byte']

        ### debag match data convert
        # self._debag_compress(match_data, version_loc, 'match_keys', 'B' )
        res.append(pack(pack_param , *match_data))

        del match_data

        for player in match_json['players']:
            player_data = []
            for key in self.api_version_keys[version_loc]['player_keys']:
                player_data.append(player[key])

            for i, ability in enumerate(player['ability_upgrades']):
                if i+1 == ability['level']:
                    for key in self.api_version_keys[version_loc]['abilyty_keys']:
                        player_data.append(ability[key])
                else:
                    raise ConversionError('leveling list broken')
            ### start ###
            ##############################################################################################
            pack_param = ''
            for key in self.api_version_keys[version_loc]['player_keys']:
                pack_param += self.api_version_keys[version_loc]['byte_codes'][key]
            abi_bytes = ''
            for key in self.api_version_keys[version_loc]['abilyty_keys']:
                abi_bytes += self.api_version_keys[version_loc]['byte_codes'][key]
            koef = (len(player_data) - len(self.api_version_keys[version_loc]['player_keys'])) // len(abi_bytes)
            pack_param += abi_bytes * koef
            # print('1 new', pack_param)
            ##############################################################################################
            ### end ###
            # pack_param = self.api_version_keys[version_loc]['player_byte'] + \
            #               self.api_version_keys[version_loc]['ability_byte'] * \
            #               (len(player_data) - len(self.api_version_keys[version_loc]['player_byte']))
            # print('2 old', pack_param)
            # print('next')
            # self._debag_compress(player_data, version_loc, 'player_keys', 'H' )
            res.append(pack(pack_param , *player_data))
        return res

    def decompress(self, compressed_match_list):
        """Convert byte_list to match json file"""
        from struct import unpack, calcsize
        for i, item in enumerate(compressed_match_list):
            if i == 0:
                version_loc = unpack('B', item)[0]
                abilyty_keys = self.api_version_keys[version_loc]['abilyty_keys']
            elif i == 1:
                ### start ###
                ###########################################################################################
                unpack_param = ''
                for key in self.api_version_keys[version_loc]['match_keys']:
                    unpack_param += self.api_version_keys[version_loc]['byte_codes'][key]
                match_data = unpack(unpack_param, item)
                ###########################################################################################
                ### end ###
                # match_data = unpack(self.api_version_keys[version_loc]['match_byte'], item)

                match_json = {}
                for ii, key in enumerate(self.api_version_keys[version_loc]['match_keys']):
                    match_json[key] = match_data[ii]
                match_json['players'] = []
                del match_data
                ### start ###
                ###########################################################################################
                abi_bytes = ''
                for key in self.api_version_keys[version_loc]['abilyty_keys']:
                    abi_bytes += self.api_version_keys[version_loc]['byte_codes'][key]
                ability_step = calcsize(abi_bytes)                  
                ###########################################################################################
                ### end ###
                # ability_step = calcsize(self.api_version_keys[version_loc]['ability_byte'])
            else:
                ### start ###
                ###########################################################################################
                unpack_param = ''
                for key in self.api_version_keys[version_loc]['player_keys']:
                    unpack_param += self.api_version_keys[version_loc]['byte_codes'][key]
                unpack_param += ((len(item) - calcsize(unpack_param)) // ability_step) * abi_bytes
                ###########################################################################################
                ### end ###
                # unpack_param = self.api_version_keys[version_loc]['player_byte'] + \
                #               ((len(item) - calcsize(self.api_version_keys[version_loc]['player_byte'])) // ability_step) * \
                #               self.api_version_keys[version_loc]['ability_byte']

                player_data = unpack(unpack_param, item)
                player_decompressed = {}
                for ii, key in enumerate(self.api_version_keys[version_loc]['player_keys']):
                    player_decompressed[key] = player_data[ii]
                player_decompressed['ability_upgrades'] = []

                lvl = 1
                ii += 1
                len_player_data = len(player_data) - 1
                while ii < len_player_data:
                    ability_dict = {'level':lvl,'time':player_data[ii],'ability':player_data[ii+1]}
                    # ability_dict.update({'time':player_data[ii],'ability':player_data[ii+1] })
                    # for y, key in enumerate(abilyty_keys):
                    #   ability_dict.update({key:player_data[ii + y]})
                    lvl += 1
                    ii += 2
                    player_decompressed['ability_upgrades'].append(ability_dict)
                match_json['players'].append(player_decompressed)
                del player_data
        if version_loc == 1:
            self._decompressed_version_upgrade(match_json)
        return match_json


"""DEBUG AND TEST ZONE"""
def _print_keys(match):
    """Print keys of match dict"""
    from hasher_vars import api_version_keys
    print('match')
    for key, value in match.items():
        if key != 'players':
            if not key in api_version_keys[2]['byte_codes']:
                print(key, value)
    print()
    print('players')    
    for player in match['players']:
        for key, value in player.items():
            if key != 'ability_upgrades':
                if not key in api_version_keys[2]['byte_codes']:
                    print(key, value)
        break
def abi_up_key(match):
    for i, player in enumerate(match['players']):
        if 'ability_upgrades' in player:
            print('player {} is OK'.format(i))
        else:
            print(player)

def time_test(zipo):
    d = Hasher()
    for i in range(10000):
        unzipo = d.decompress(zipo)
    return


def main():
    """Some test scripts"""
    import dota2api
    import timeit
    import time
    import cProfile

    api = dota2api.Initialise("steam_api_key")
    match = api.get_match_details(match_id=2970232655)
    abi_up_key(match)
    d = Hasher()
    start_match = match['start_time']
    # zipo = d.compress(match)
    # print(d.decompress(zipo))

    # for i, player in enumerate(match['players']):
    #   for key, val in player.items():
    #       if type(val) == int:
    #           if val > 255:
    #               print(i, key, val)

    # _print_keys(match)
    
    # print(match)
    print()

    from datetime import datetime, date, time
    date_ = datetime.fromtimestamp(start_match) 

    print(date_)

    # print('zipo')
    # print(zipo)
    # k = Hasher()

    # cProfile.run('time_test(zipo)', sort = 'time')

    # from struct import calcsize
    # print('H', calcsize('H'))
    # print('L', calcsize('L'))
    # print('B', calcsize('B'))
    # print('I', calcsize('I'))

    # print('size')
    # import sys
    # print(sys.getsizeof(zipo))
    # import pickle
    # with open('match.pickle', 'wb') as f:
    #   pickle.dump( zipo, f)

    ### simulate dota 2 api version = 1
    # del match['radiant_score']
    # del match['dire_score']
    # for player in match['players']:
    #   [player.pop('backpack_{}'.format(x)) for x in range(2)]
    #   del player['scaled_hero_damage']
    #   del player['scaled_tower_damage']
    #   del player['scaled_hero_healing']
    # print('unzipo')
    # print('radiant_score', unzipo['radiant_score'])
    # print('dire_score', unzipo['dire_score']) 
    # [print('hero_damage', player['hero_damage'], player['scaled_hero_damage'])  for player in unzipo['players']]
    # [print('tower_damage', player['tower_damage'], player['scaled_tower_damage']) for player in unzipo['players']]
    # [print('hero_healing',player['hero_healing'], player['scaled_hero_healing']) for player in unzipo['players']]
if __name__ == "__main__":
    main()
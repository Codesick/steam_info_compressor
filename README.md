# README #

### Hasher ###

Class Hasher used to compression match data from dota2api before that have store in DataBase(DB), 
and decopression data from DB to source format

### Examples: ###

    # take a match ( for example from dota2api)
    import dota2api
    api = dota2api.Initialise("steam_api_key")
    match = api.get_match_details(match_id=2919000874)

    # match is a dict() object
    hasher = Hasher()
    # compresed_match == list()
    compresed_match = hasher(match)

    # decompresed_match == dict()
    decompresed_match = hasher(compresed_match)

``` warning: decompresed_match has not some keys of source match ```
